def checkSubarraySum(nums, k):
    prefix_sums = {0: -1}
    prefix_sum = 0

    for i in range(len(nums)):
        prefix_sum += nums[i]
        mod = prefix_sum % k

        if mod not in prefix_sums:
            prefix_sums[mod] = i
        else:
            if i - prefix_sums[mod] >= 2:
                return True

    return False

nums = [23, 2, 4, 6, 7]
k = 6
print(checkSubarraySum(nums, k))

nums = [23, 2, 6, 4, 7]
k = 6
print(checkSubarraySum(nums, k))

nums = [23, 2, 6, 4, 7]
k = 13
print(checkSubarraySum(nums, k))