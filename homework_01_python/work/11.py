def is_unique_string(s):
    seen = set()

    for ch in s:
        if ch in seen:
            return False
        seen.add(ch)

    return True

x=input("输入: ")
print("输出:",is_unique_string(x))
