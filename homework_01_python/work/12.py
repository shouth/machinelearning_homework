def maxNumberOfBalloons(s):
    char_counts = {}
    for ch in s:
        char_counts[ch] = char_counts.get(ch, 0) + 1

    max_balloons = float('inf')
    for ch in 'balloon':
        if ch not in char_counts or char_counts[ch] < 'balloon'.count(ch):
            max_balloons = 0
            break
        max_balloons = min(max_balloons, char_counts[ch] // 'balloon'.count(ch))

    return max_balloons

x=input("输入：")
print(maxNumberOfBalloons(x))
