import random
import string

def generate_activation_code(length):
    characters = string.ascii_letters + string.digits
    code = ''.join(random.choice(characters) for _ in range(length))
    return code

num_codes = 200
code_length = 10

activation_codes = []
for _ in range(num_codes):
    code = generate_activation_code(code_length)
    activation_codes.append(code)

for code in activation_codes:
    print(code)