import os

def find_files(directory, extension):
    files = []
    for root, _, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith(extension):
                files.append(os.path.join(root, filename))
    return files

directory = 'C:\\'
extension = '.dll'

result = find_files(directory, extension)

for file in result:
    print(file)