import os

def count_lines(directory, file_extension):
    total_lines = 0
    code_lines = 0
    comment_lines = 0
    blank_lines = 0

    for root, _, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith(file_extension):
                file_path = os.path.join(root, filename)
                with open(file_path, 'r', encoding='utf-8') as file:
                    lines = file.readlines()
                    total_lines += len(lines)

                    for line in lines:
                        line = line.strip()

                        if line == '':
                            blank_lines += 1
                        elif line.startswith('//') or line.startswith('#'):
                            comment_lines += 1
                        elif line.startswith('/*') or line.endswith('*/'):
                            comment_lines += 1
                        else:
                            code_lines += 1

    return total_lines, code_lines, comment_lines, blank_lines

current_directory = os.path.dirname(os.path.abspath(__file__))
file_extension = '.py'
total, code, comments, blank = count_lines(current_directory, file_extension)
print("python程序")
print(f"总行数: {total}")
print(f"代码行数: {code}")
print(f"注释行数: {comments}")
print(f"空行数: {blank}")
# 指定文件类型
file_extension = '.c'

# 统计代码行数
total, code, comments, blank = count_lines(current_directory, file_extension)

# 打印统计结果
print("c程序")
print(f"总行数: {total}")
print(f"代码行数: {code}")
print(f"注释行数: {comments}")
print(f"空行数: {blank}")