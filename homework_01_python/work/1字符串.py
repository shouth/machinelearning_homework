def countwords(article):
    article = article.lower()
    article = ''.join(c if c.isalnum() else ' ' for c in article)
    words = article.split()
    word_count = {}
    for word in words:
        if word in word_count:
            word_count[word] += 1
        else:
            word_count[word] = 1

    return word_count

article = input("请输入文章: ")
word_count = countwords(article)

# 打印结果
for word, count in word_count.items():
    print(f"{word}: {count}")