numbers = []
for i in range(1, 5):
    for j in range(1, 5):
        for k in range(1, 5):
             if i != j and j != k and i != k:
                 number = i * 100 + j * 10 + k
                 numbers.append(number)

print(f"共有{len(numbers)}个满足条件的三位数:")
for number in numbers:
    print(number)