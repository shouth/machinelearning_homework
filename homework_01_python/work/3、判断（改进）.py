def calculate_bonus(profit):
    thresholds = [1000000, 600000, 400000, 200000, 100000, 0]
    rates = [0.01, 0.015, 0.03, 0.05, 0.075, 0.1]
    bonus = 0

    for i in range(len(thresholds)):
        if profit > thresholds[i]:
            bonus += (profit - thresholds[i]) * rates[i]
            profit = thresholds[i]

    return bonus

profit = float(input("请输入当月利润（单位：万元）: "))
bonus = calculate_bonus(profit)
print(f"应发放奖金总数为：{bonus}万元")