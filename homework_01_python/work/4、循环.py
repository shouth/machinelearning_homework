for i in range(1, 10):
    for j in range(1, 10):
        result = i * j
        print(f"{i:2d} x {j:2d} = {result:2d}", end="  ")
    print()