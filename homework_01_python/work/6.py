numbers = input("请输入数字列表，用逗号分隔：")
numbers = [int(num) for num in numbers.split(",")]

sorted_numbers = sorted(enumerate(numbers), key=lambda x: x[1], reverse=True)

sorted_values = [value for _, value in sorted_numbers]
sorted_indices = [index for index, _ in sorted_numbers]

print("排序后的元素:", ', '.join(map(str, sorted_values)))
print("排序后的下标:", ', '.join(map(str, sorted_indices)))