#在这段代码中，我定义了一个名为quicksort()的递归函数，用于对列表进行快速排序。
# 函数首先选择一个基准元素（这里选择中间元素），然后将列表分为比基准元素小、等于基准元素和大于基准元素的三个部分。
# 接着，递归地对较小和较大的部分进行排序，并将它们与相等的元素合并起来。

def quicksort(arr):
    if len(arr) <= 1:
        return arr
    pivot = arr[len(arr) // 2]
    smaller = [x for x in arr if x < pivot]
    equal = [x for x in arr if x == pivot]
    greater = [x for x in arr if x > pivot]
    return quicksort(greater) + equal + quicksort(smaller)

numbers = input("请输入数字列表，用逗号分隔：")
numbers = [int(num) for num in numbers.split(",")]

sorted_numbers = quicksort(numbers)
sorted_indices = [numbers.index(num) for num in sorted_numbers]

print("排序后的元素:", ', '.join(map(str, sorted_numbers)))
print("排序后的下标:", ', '.join(map(str, sorted_indices)))