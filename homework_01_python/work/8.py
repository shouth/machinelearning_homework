import math

def find_perfect_numbers():
    perfect_numbers = []

    for num in range(1, 1001):
        factors = []
        sqrt = int(math.sqrt(num))

        for i in range(1, sqrt + 1):
            if num % i == 0:
                factors.append(i)
                factors.append(num // i)

        sum_of_factors = sum(factors) - num

        if sum_of_factors == num:
            perfect_numbers.append(num)

    return perfect_numbers

perfect_numbers = find_perfect_numbers()

print("1000以内的完数：")
for number in perfect_numbers:
    print(number)