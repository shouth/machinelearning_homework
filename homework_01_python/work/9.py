def is_happy(n):
    def get_next(n):
        total_sum = 0
        while n > 0:
            digit = n % 10
            total_sum += digit ** 2
            n //= 10
        return total_sum

    visited = set()
    while n != 1 and n not in visited:
        visited.add(n)
        n = get_next(n)

    return n == 1

# 测试示例
x=int(input("请输入："))
print(is_happy(x))