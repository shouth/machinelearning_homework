import array

import numpy as np
x= np.array([[10, 34, 54, 23],
                  [31, 87, 53, 68],
                  [98, 49, 25, 11],
                  [84, 32, 67, 88]])
row,col=x.shape
new_row=row+2
new_col=col+2
new_x=np.zeros((new_row,new_col))
new_x[1:-1,1:-1]=x
print(new_x)