import numpy as np
random= np.random.rand(10, 10)

# 找出数组中的最大值和最小值
max= np.max(random)
min= np.min(random)

print("随机数组:")
print(random)
print("最大值:", max)
print("最小值:", min)