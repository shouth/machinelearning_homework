import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-5, 5, 30)
plt.plot(x, x**2+10)


m = np.linspace(-5, 5, 10)
for i in range(10):
    plt.plot([m[i],m[i]],[m[i]**2+10,0])
plt.xlim((-6, 6))
plt.ylim((0, 70))
plt.show()