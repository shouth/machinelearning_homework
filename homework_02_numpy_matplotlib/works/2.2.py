import numpy as np
import matplotlib.pyplot as plt

# 定义函数
def f(x):
    return np.sin(x-2)**2 * np.exp(-x**2)

# 定义x轴的取值范围
x = np.linspace(0, 2, 100)

# 计算对应的y轴值
y = f(x)

# 绘制函数图形
plt.plot(x, y)

# 添加标题和轴标签
plt.title('Plot of f(x) = sin^2(x-2) * e^(-x^2)')
plt.xlabel('x')
plt.ylabel('f(x)')

# 显示图形
plt.show()