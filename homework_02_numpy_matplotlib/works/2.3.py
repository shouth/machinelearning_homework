import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def random_walk_2D(num_steps):
    # 初始位置为原点
    x = [0]
    y = [0]

    for _ in range(num_steps):
        step_x = np.random.choice([-1, 1])
        step_y = np.random.choice([-1, 1])
        x.append(x[-1] + step_x)
        y.append(y[-1] + step_y)

    return x, y


num_steps = 1000
x, y = random_walk_2D(num_steps)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(x, y, range(num_steps+1))
ax.set_title('3D Random Walk')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('Steps')
plt.show()