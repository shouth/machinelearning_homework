import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler


def kMeans(data, k, max_iterations=100):
    centroids = data[np.random.choice(range(len(data)), k, replace=False)]

    for _ in range(max_iterations):
        distances = np.linalg.norm(data[:, np.newaxis] - centroids, axis=2)
        labels = np.argmin(distances, axis=1)
        new_centroids = np.array([data[labels == i].mean(axis=0) for i in range(k)])

        if np.all(centroids == new_centroids):
            break

        centroids = new_centroids

    return labels


# 加载数据
data = pd.read_csv('dataset_circles.csv', header=None)

# 聚类数量
k = 3

# 调用kMeans方法进行聚类
labels = kMeans(data.values[:, :-1], k)

# 将数据可视化
plt.scatter(data[0], data[1], c=labels)
plt.xlabel('x')
plt.ylabel('y')
plt.title('kMeans Clustering')
plt.show()

# 标准化处理
scaler = StandardScaler()
scaled_data = scaler.fit_transform(data.values[:, :-1])

# 调用kMeans方法进行聚类
labels = kMeans(scaled_data, k)

# 将变换后的数据可视化
plt.scatter(scaled_data[:, 0], scaled_data[:, 1], c=labels)
plt.xlabel('x (scaled)')
plt.ylabel('y (scaled)')
plt.title('kMeans Clustering (Scaled Data)')
plt.show()