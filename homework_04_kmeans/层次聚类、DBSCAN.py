import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import AgglomerativeClustering, DBSCAN

# 加载数据集
data = pd.read_csv('dataset_circles.csv', header=None)
X = data.iloc[:, 0:2].values

# 进行数据变换
scaler = StandardScaler()
X_transformed = scaler.fit_transform(X)

# 层次聚类
agglomerative = AgglomerativeClustering(n_clusters=2)
labels_agglomerative = agglomerative.fit_predict(X_transformed)

# 可视化层次聚类结果
plt.scatter(X_transformed[:, 0], X_transformed[:, 1], c=labels_agglomerative)
plt.title('Agglomerative Clustering')
plt.xlabel('X (Transformed)')
plt.ylabel('Y (Transformed)')
plt.show()

# DBSCAN聚类
dbscan = DBSCAN(eps=0.3, min_samples=5)
labels_dbscan = dbscan.fit_predict(X_transformed)

# 可视化DBSCAN聚类结果
plt.scatter(X_transformed[:, 0], X_transformed[:, 1], c=labels_dbscan)
plt.title('DBSCAN Clustering')
plt.xlabel('X (Transformed)')
plt.ylabel('Y (Transformed)')
plt.show()