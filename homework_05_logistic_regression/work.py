
#输出模型的准确率，并显示错误分类的样本的图像
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


def sigmoid(z):
    return 1 / (1 + np.exp(-z))


class LogisticRegression:
    def __init__(self, num_classes, num_features, learning_rate=0.1, num_iterations=1000):
        self.num_classes = num_classes
        self.num_features = num_features
        self.learning_rate = learning_rate
        self.num_iterations = num_iterations
        self.weights = np.zeros((num_features, num_classes))

    def train(self, X, y):
        for _ in range(self.num_iterations):
            scores = np.dot(X, self.weights)
            predictions = sigmoid(scores)
            error = predictions - y
            gradient = np.dot(X.T, error)
            self.weights -= self.learning_rate * gradient

    def predict(self, X):
        scores = np.dot(X, self.weights)
        predictions = sigmoid(scores)
        return np.argmax(predictions, axis=1)


# Load data
digits = load_digits()
X = digits.data
y = digits.target

# Split data into train and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Train logistic regression model
model = LogisticRegression(num_classes=10, num_features=X.shape[1])
model.train(X_train, np.eye(10)[y_train])

# Predict on test set
y_pred = model.predict(X_test)

# Calculate accuracy
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)


# Visualization of misclassified samples
misclassified_samples = X_test[y_test != y_pred]
misclassified_targets = y_test[y_test != y_pred]
misclassified_predictions = y_pred[y_test != y_pred]

fig = plt.figure(figsize=(6, 6))
fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05)

for i in range(len(misclassified_samples)):
    ax = fig.add_subplot(8, 8, i + 1, xticks=[], yticks=[])
    ax.imshow(misclassified_samples[i].reshape(8, 8), cmap=plt.cm.binary)
    ax.text(0, 7, str(misclassified_targets[i]), color='red')
    ax.text(0, 1, str(misclassified_predictions[i]), color='blue')

plt.show()